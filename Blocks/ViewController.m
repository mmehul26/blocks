//
//  ViewController.m
//  Blocks
//
//  Created by Mehul Makwana on 01/09/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "ViewController.h"

typedef NSString* (^stringBlock) (NSString*);

@interface ViewController ()

@property (copy) void (^blockProperty)(void);


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//Declare Block
    void (^simpleBlock)(void) = ^{
        NSLog(@"This is a block");
    };
    //invoke the block
    simpleBlock();
    //Prints This is a block
    
    
//Blocks Take Arguments and Return Values
    double (^multiplyTwoValues)(double, double) =
    ^(double firstValue, double secondValue) {
        return firstValue * secondValue;
    };
    
    double result = multiplyTwoValues(2,4);
    
    NSLog(@"The result is %f", result);
    //Prints The result is 8.000000

    
//Blocks Can Capture Values from the Enclosing Scope
    int anInteger = 42;
    
    void (^testBlock)(void) = ^{
        NSLog(@"Integer is: %i", anInteger);
    };
    
    anInteger = 84;
    testBlock();
    //Prints Integer is: 42
    //It means that the block cannot change the value of the original variable, or even the captured value (it’s captured as a const variable).
   
    
//Use __block Variables to Share Storage
    __block int anInteger1 = 42;
    
    void (^testBlock1)(void) = ^{
        NSLog(@"Integer is: %i", anInteger1);
    };
    
    anInteger1 = 84;
    
    testBlock1();
    //Prints Integer is: 84

    //It also means that the block can modify the original value, like this:
    __block int anInteger2 = 42;
    
    void (^testBlock2)(void) = ^{
        NSLog(@"Integer is: %i", anInteger2);
        anInteger2 = 100;
    };
    
    testBlock2();
    NSLog(@"Value of original variable is now: %i", anInteger2);
    

//You Can Pass Blocks as Arguments to Methods or Functions
    [self beginTaskWithName:@"MyTask" completion:^{
        NSLog(@"The task is complete");
    }];

    
//Use Type Definitions to SimplifyBlock Syntax
    stringBlock typedefString = ^(NSString * str){
        return [NSString stringWithFormat:@"Mehul %@",str];
    };
    
    typedefString(@"Makwana");
    NSLog(@"%@",typedefString(@"Makwana"));
    //Prints Mehul Makwana
    
    
//Objects Use Properties to Keep Track of Blocks

    //Note: You should specify copy as the property attribute, because a block needs to be copied to keep track of its captured state outside of the original scope.
    self.blockProperty = ^{
        NSLog(@"This is block property");
    };
    self.blockProperty();
    
    
//Avoid Strong Reference Cycles when Capturing self

    self.blockProperty = ^{
        [self beginTaskWithName:@"Mehul" completion:^{
            NSLog(@"Capturing 'self' strongly in this block is likely to lead to a retain cycle");
        }];     // capturing a strong reference to self
                // creates a strong reference cycle
    };
    self.blockProperty();
    //The compiler will warn you for a simple example like this, but a more complex example might involve multiple strong references between objects to create the cycle, making it more difficult to diagnose.
    
    ViewController * __weak weakSelf = self;
    self.blockProperty = ^{
        [weakSelf viewDidDisappear:TRUE];   // capture the weak reference
                                            // to avoid the reference cycle
    };
    //By capturing the weak pointer to self, the block won’t maintain a strong relationship back to the ViewController object. If that object is deallocated before the block is called, the weakSelf pointer will simply be set to nil.
    

//Blocks Can Simplify Enumeration for Collections
    NSArray *arras = @[@"str1",@"str2",@"str3",@"str4"];
    
    [arras enumerateObjectsUsingBlock:^ (id obj, NSUInteger idx, BOOL *stop) {
        NSLog(@"Object at index %lu is %@", idx, obj);
    }];
    //prints    Object at index 0 is str1
            //  Object at index 1 is str2
            //  Object at index 2 is str3
            //  Object at index 3 is str4

    
//Blocks Can Simplify Concurrent Tasks
    // schedule task on main queue:

    
//Schedule Blocks on Dispatch Queues with Grand Central Dispatch
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    dispatch_async(queue, ^{
        NSLog(@"Block for asynchronous execution");
    });
    
    //The dispatch_async() function returns immediately, without waiting for the block to be invoked
    
   //  The dispatch_sync() function doesn’t return until the block has completed execution
    
    
}

- (void)beginTaskWithName:(NSString *)name completion:(void(^)(void))callback
{
    callback();
    
}

@end
